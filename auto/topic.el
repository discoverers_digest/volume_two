(TeX-add-style-hook
 "topic"
 (lambda ()
   (LaTeX-add-labels
    "fig:editorial"
    "sec:relations"
    "lst:relationships"
    "sec:wikisim"))
 :latex)

