(TeX-add-style-hook
 "editorial"
 (lambda ()
   (LaTeX-add-labels
    "fig:editorial"
    "img:kasparov"
    "link:watson"
    "img:spock"
    "link:extent_model"))
 :latex)

